#!/usr/bin/python3
# -*- coding: utf8
import sys
import os
import subprocess

# 需要检查当前执行的commit是否是最新的分支上的，否则不执行构建

# 首先需要判断当前commit是否是分支上的
commit = os.popen('git rev-parse HEAD').read().strip()
origins = os.popen('git ls-remote origin').read().strip()
isHeadingBranch = False

if commit in origins:
    origins = origins.split('\n')
    for x in origins:
        if commit in x and 'refs/heads/' in x:
            branch = x.split('\trefs/heads/')[1]
            isHeadingBranch = True

if isHeadingBranch:
    print('在分支末端，ci-runner可以提交。')
    subprocess.call('echo newcon >> test.txt', shell=True)
    subprocess.call('git add .', shell=True)
    subprocess.call(
        'git commit -m "由gilab-runner自动生成, 部署建议使用这个" -m "[skip ci]"', shell=True)
    subprocess.call('git push origin HEAD:{}'.format(branch), shell=True)
else:
    print('未检测到当前分支的情况，ci-runner将不会提交，避免错误。')
